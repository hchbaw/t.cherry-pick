;; Gaucheのリファレンスをちょこちょこつまみぐい
;; 写経したり、動かしてみたり…
(select-module user)
(define-module t.cherry-pick.gauche-ref
  (use srfi-1 :prefix srfi-1:))
(select-module t.cherry-pick.gauche-ref)

;;; List constructors
;; xcons
;; “高階手続きへ渡すのに便利です。” そのまま、知らなかった！
(let ()
  (define (foldcall fold-proc kons)
    (fold-proc kons '() '(a b c)))

  (values
   (foldcall fold cons)
   (foldcall fold-left srfi-1:xcons)))

;; list-tabulate
(srfi-1:list-tabulate 4 values)

(let ()
  (define (list-tabulate len proc)
    (let loop ((i (- len 1)) (acc '()))
      (if (< i 0)
        acc
        (loop (- i 1) (cons (proc i) acc)))))
  #;(define (list-tabulate len proc)
    (do ((i (- len 1) (- i 1))
         (acc '() (cons (proc i) acc)))
        ((< i 0) acc)))

  (values
   (list-tabulate 4 values)
   (list-tabulate 4 number->string)))

;; circular-list elt1 elt2 …
;; (circular-list 'z 'q) ⇒ (z q z q z q …)
;; last-pair (Gauche has this.)
(let ()
  ;; REDEFINED! SHADOWING
  (define (last-pair xs)
    (if (pair? (cdr xs))
      (last-pair (cdr xs))
      xs))
  (define (circular-list x . xs)
    (rlet1 ret (cons x xs)
      (set-cdr! (last-pair ret) ret)))

  (values
   (circular-list 'z 'q)
   (circular-list? (circular-list 'z 'q))))

;;; List predicates
;; not-pair? x
(let ()
  (define not-pair? (complement pair?))

  (values
   (not-pair? 'x)
   (not-pair? '())
   (not-pair? '(a . b))
   (not-pair? '(a b c))))

;; list= elt= list …
;; (eq? x y) ⇒ (elt= x y)
;; 二つのリストが同値かの判定にはnull-list?で最後まで辿ったかを見ている
;; パラメータのリストを辿るのにはnull?を使っていて、引数リストを取る関数定義
;; なので自然な感じがある
(let ()
  (define (list= =? . xxs)
    (or (null? xxs)
        (let loop1 ((as (car xxs))
                    (xxs (cdr xxs)))
          (or (null? xxs)
              (let ((bs (car xxs))
                    (xxs (cdr xxs)))
                (if (eq? as bs)
                  (loop1 bs xxs)
                  (let loop2 ((as as)
                              (cs bs))
                    ;; bs キャプチャして置いておく理由
                    ;; as と cs か同値かをそれぞれ cdr を辿って見て、
                    ;; その後 bs と xxs を loop1 で辿る
                    (if (null-list? as)
                      (and (null-list? cs)
                           (loop1 bs xxs))
                      (and (not (null-list? cs))
                           (=? (car as) (car cs))
                           (loop2 (cdr as) (cdr cs)))))))))))

  (values
   (list= eq? '(a) '(a) '(a))
   (list= eqv? '(1 2) '(1 2) '(1 2 3))))

;;; List selectors
;; first 〜 tenth まで
(let ()
  (define xs (srfi-1:list-tabulate 10 (^n (+ n 1))))

  (list
   (srfi-1:first xs)
   (srfi-1:second xs)
   (srfi-1:third xs)
   (srfi-1:fourth xs)
   (srfi-1:fifth xs)
   (srfi-1:sixth xs)
   (srfi-1:seventh xs)
   (srfi-1:eighth xs)
   (srfi-1:ninth xs)
   (srfi-1:tenth xs)))

;; car+cdr
(srfi-1:car+cdr '(a . b))

;;; List miscellaneous routines
;; zip clist1 clist2 …
(let ()
  ;; 最低一つのリストを取るように
  (define (zip list1 . more-lists)
    (apply map list list1 more-lists))

  (zip '(a b c) '(1 2 3) '(one two three)))

;; unzip
;; unzip は 1 から 5 まである…どう定義すると後々でも腑に落ちるのか
(srfi-1:unzip1 '((a b) (1 2) (one two)))
(let ()
  (define unzip1 (map$ car))

  (unzip1 '((a b) (1 2) (one two))))

(define-constant unzip-lists '((1 2 3 4 5)
                               (a b c d e)
                               (one two three four five)))

;; unzip2
(let ()
  (define (unzip2 xxs)
    (if (null-list? xxs)
      (values xxs xxs)   ; ここと
      (let1 xs (car xxs)
        (receive (a b) (unzip2 (cdr xxs)) ; ここ、あと、
          (values (cons (car xs) a)       ; この辺
                  (cons (cadr xs) b)))))) 

  (unzip2 unzip-lists))

(let ()
  (define (unzip2 xxs)
    ;; 再帰でそれぞれを呼ぶように書くのがつらいのでrecurと置く
    (let recur ((xxs xxs))
      (if (null-list? xxs)
        (values xxs xxs)  ; ここ
        (let1 xs (car xxs)
          (receive (a b) (recur (cdr xxs))
            (values (cons (car xs) a)        ; この辺
                    (cons (cadr xs) b)))))))
  (define (unzip3 xxs)
    (let recur ((xxs xxs))
      (if (null-list? xxs)
        (values xxs xxs xxs)  ; ここ
        (let1 xs (car xxs)
          (receive (a b c) (recur (cdr xxs))
            (values (cons (car xs) a)        ; この辺
                    (cons (cadr xs) b)
                    (cons (caddr xs) c)))))))
  ;; 5とかまで続けていくのがつらい気がする…

  (unzip3 unzip-lists))

(let ()
  ;; 脱線。末尾再帰で
  (define (unzip2 xxs)
    (define (loop xxs k)
      (if (null-list? xxs)
        (k xxs xxs)
        (let1 xs (car xxs)
          (loop (cdr xxs) (lambda (a b)
                            (k (cons (car xs) a)
                               (cons (cadr xs) b)))))))
    (loop xxs values))

  (unzip2 unzip-lists))

(let ()
  ;; 抽象化してみるが…
  (define (unzipcall tail-gen step-proc xxs)
    (let recur ((xxs xxs))
      (if (null-list? xxs)
        (tail-gen xxs)
        (let1 xs (car xxs)
          (call-with-values
            (lambda () (recur (cdr xxs)))
            (lambda args (apply step-proc xs args)))))))
  (define unzip2
    (pa$ unzipcall
         (lambda _ (values '() '()))
         (lambda (xs a b)
           (values (cons (car xs) a)
                   (cons (cadr xs) b)))))

  (unzip2 unzip-lists))

;; list-ref を使って伝統的なマクロで
(define-macro (define-unzipn n)
  (let ((syms (srfi-1:list-tabulate n (^_ (gensym)))))
    `(define (,(string->symbol #`"unzip,|n|") xxs)
       (let recur ((xxs xxs))
         (if (null-list? xxs)
           (values ,@(make-list n 'xxs)) 
           (let1 xs (car xxs)
             (receive ,syms (recur (cdr xxs))
               (values
                ,@(map (lambda (i s) `(cons (list-ref xs ,i) ,s))
                       (iota n)
                       syms)))))))))
(define-macro (define-unzips!)
  `(begin
     ,@(map (lambda (n) `(define-unzipn ,n)) '(2 3 4 5))))

(define-unzips!)

(let ((xs unzip-lists))
  (values
   `(**2 ,(values->list (unzip2 xs)))
   `(**3 ,(values->list (unzip3 xs)))
   `(**4 ,(values->list (unzip4 xs)))
   `(**5 ,(values->list (unzip5 xs)))))

;; つらい部分がどっかに出てしまうというのが正直なところ。自分では、伝統的マ
;; クロで最初から書けるとは思えないし、かといって何個も似たようなコードが続
;; くのは気が引けるしで、結論が出せないなあ。
